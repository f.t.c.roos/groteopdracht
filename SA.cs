﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GroteOpdracht
{
    class SA
    {
        public static Random random = new Random();
        public static double proba;

        // Adds a random insufficiently handled order to a random route
        public void Add()
        {

            if (Program.schedule.declinedOrders.Count == 0)
            {
                return; // there are no more additional orders to handle
            }

            int dayIndex = random.Next(0, 10);
            int orderIndex = random.Next(0, Program.schedule.declinedOrders.Count);

            Order order = Program.schedule.declinedOrders[orderIndex];
            DaySchedule day = Program.schedule.daySchedules[dayIndex];
            
            int routeIndex = random.Next(0, day.route.orders.Count);
                        
            if (!day.IsOrderAllowed(order))
            {
                return; // order is not allowed on this day
            }

            float oldScore = Program.schedule.Score();

            day.route.InsertAt(routeIndex, order);
            Program.schedule.declinedOrders.Remove(order);

            float newScore = Program.schedule.Score();
            float newTime = day.route.Time();
            float newCapacity = day.route.MaxLoad(day.route.Loop(order));

            proba = random.NextDouble();
            float delta = newScore - oldScore; // als delta positief is dan was oldscore beter is 
            if (delta > 0 || proba < Math.Exp(delta / Program.temperature) || newTime > 43200 || newCapacity > Truck.maxCapacity)
            {
                day.route.Remove(order);
                Program.schedule.declinedOrders.Add(order);
            } 
        }

        // Removes a random order from a random route
        public void Remove()
        {
            int dayIndex = random.Next(0, 10);
            DaySchedule day = Program.schedule.daySchedules[dayIndex];

            if (day.route.orders.Count == 0)
            {
                return; // there are no orders to be removed on this route
            }

            int orderIndex = random.Next(0, day.route.orders.Count); // count - 1
            Order order = day.route.orders[orderIndex];

            if (order == Program.dump)
            {
                return; // we do not want to return the dump in the first place
            }

            float oldScore = Program.schedule.Score();

            day.route.Remove(order);
            Program.schedule.declinedOrders.Add(order);

            float newScore = Program.schedule.Score();

            proba = random.NextDouble();
            float delta = newScore - oldScore;
            if (delta > 0 || proba < Math.Exp(delta / Program.temperature))
            {
                day.route.InsertAt(orderIndex, order);
                Program.schedule.declinedOrders.Remove(order);
            }
        }


        // Swaps two neighboring orders planned on the same day
        public void SwapNeighbors()
        {
            int dayIndex = random.Next(0, 10);
            DaySchedule day = Program.schedule.daySchedules[dayIndex];

            if (day.route.orders.Count <= 1)
            {
                return; // nothing to swap, list of orders is too small
            }

            int index1 = random.Next(0, day.route.orders.Count - 1);
            int index2 = index1 + 1;

            if (day.route.orders[index1] == Program.dump || day.route.orders[index2] == Program.dump)
            {
                return; // nothing to swap, we found the dump
            }

            //float oldTime = day.route.Time();
            float oldScore = Program.schedule.Score();

            Order order1 = day.route.orders[index1];
            Order order2 = day.route.orders[index2];

            day.route.orders.RemoveAt(index1);
            day.route.orders.Insert(index1, order2);
            day.route.orders.RemoveAt(index2);
            day.route.orders.Insert(index2, order1);

            //float newTime = day.route.Time();
            float newScore = Program.schedule.Score();
            float newTime = day.route.Time();

            proba = random.NextDouble();
            float delta = newScore - oldScore; // als delta > 0 , dan was oldtime beter dus dan moet je reverten
            if (delta > 0 || proba < Math.Exp(delta / Program.temperature) || newTime > 43200) // new route is not feasible, revert changes
            {
                day.route.orders.RemoveAt(index1);
                day.route.orders.Insert(index1, order1);
                day.route.orders.RemoveAt(index2);
                day.route.orders.Insert(index2, order2);
            }
        }


        // Swaps two random orders within a day
        public void SwapLocal()
        {
            int dayIndex = random.Next(0, 10);
            DaySchedule day = Program.schedule.daySchedules[dayIndex];

            if (day.route.orders.Count == 0)
            {
                return; // nothing to swap, list of orders is empty
            }

            int index1 = random.Next(0, day.route.orders.Count - 1);
            int index2 = random.Next(0, day.route.orders.Count - 1);

            if (day.route.orders[index1] == Program.dump || day.route.orders[index2] == Program.dump)
            {
                return; // nothing to swap, we found the dump
            }

            float oldTime = day.route.Time();
            float oldScore = Program.schedule.Score();

            Order order1 = day.route.orders[index1];
            Order order2 = day.route.orders[index2];

            // Make sure that the maximum capacity of the truck is not exceeded
            int oldMaxLoad1 = day.route.MaxLoad(day.route.Loop(order1));
            int oldMaxLoad2 = day.route.MaxLoad(day.route.Loop(order2));

            int newMaxLoad1 = oldMaxLoad1 - order1.totalVolume + order2.totalVolume;
            int newMaxLoad2 = oldMaxLoad2 - order2.totalVolume + order1.totalVolume;

            // Make sure the time limit is not exceeded
            day.route.orders.RemoveAt(index1);
            day.route.orders.Insert(index1, order2);
            day.route.orders.RemoveAt(index2);
            day.route.orders.Insert(index2, order1);

            float newTime = day.route.Time();
            float newScore = Program.schedule.Score();

            proba = random.NextDouble();
            float delta = newScore - oldScore; // als delta > 0 , dan was oldtime beter dus dan moet je reverten
            if (delta > 0 || proba < Math.Exp(delta / Program.temperature) || newTime > oldTime || newMaxLoad1 > Truck.maxCapacity || newMaxLoad2 > Truck.maxCapacity) // new route is not feasible, revert changes
            {
                day.route.orders.RemoveAt(index1);
                day.route.orders.Insert(index1, order1);
                day.route.orders.RemoveAt(index2);
                day.route.orders.Insert(index2, order2);
            }
        }


        // Swaps two orders between two random days
        public void SwapGlobal()
        {
            int dayIndex1 = random.Next(0, 10);
            int dayIndex2 = random.Next(0, 10);

            DaySchedule day1 = Program.schedule.daySchedules[dayIndex1];
            DaySchedule day2 = Program.schedule.daySchedules[dayIndex2];

            if (day1.route.orders.Count == 0 || day2.route.orders.Count == 0)
            {
                return; // nothing to swap, one of the lists of orders is empty
            }

            int index1 = random.Next(0, day1.route.orders.Count - 1);
            int index2 = random.Next(0, day2.route.orders.Count - 1);

            if (day1.route.orders[index1] == Program.dump || day2.route.orders[index2] == Program.dump)
            {
                return; // nothing to swap, we found the dump
            }

            float oldTime1 = day1.route.Time();
            float oldTime2 = day2.route.Time();

            Order order1 = day1.route.orders[index1];
            Order order2 = day2.route.orders[index2];

            // Handle multifrequency orders
            if(!(order1.frequency == 1 && order2.frequency == 1) && day1 != day2)
            {
                //order1.SetFlagFalse(day1.dayNumber);
                //order2.SetFlagFalse(day2.dayNumber);

                bool orderSwapAllowed = day2.IsOrderAllowed(order1) && day1.IsOrderAllowed(order2);

                //order1.SetFlagTrue(day1.dayNumber);
                //order2.SetFlagTrue(day2.dayNumber);

                if (!orderSwapAllowed)
                {
                    return; // swapping these orders results in an infeasable solution
                }
            }

            // Make sure that the maximum capacity of the truck is not exceeded
            int oldMaxLoad1 = day1.route.MaxLoad(day1.route.Loop(order1));
            int oldMaxLoad2 = day2.route.MaxLoad(day2.route.Loop(order2));

            float oldScore1 = Program.schedule.Score();
            float oldScore2 = Program.schedule.Score();

            int newMaxLoad1 = oldMaxLoad1 - order1.totalVolume + order2.totalVolume;
            int newMaxLoad2 = oldMaxLoad2 - order2.totalVolume + order1.totalVolume; 

            // Make sure the time limit is not exceeded
            day1.route.orders.RemoveAt(index1);
            day1.route.orders.Insert(index1, order2);

            day2.route.orders.RemoveAt(index2);
            day2.route.orders.Insert(index2, order1);
            

            float newTime1 = day1.route.Time();
            float newTime2 = day2.route.Time();

            float newScore1 = Program.schedule.Score();
            float newScore2 = Program.schedule.Score();

            proba = random.NextDouble();
            float delta = (newScore1 + newScore2) - (oldScore1 + oldScore2); // als delta > 0 , dan was oldtime beter dus dan moet je reverten
            if (delta > 0 || proba < Math.Exp(delta / Program.temperature) || newTime1 + newTime2 > oldTime1 + oldTime2 || newTime1 > 43200 || newTime2 > 43200 || newMaxLoad1 > Truck.maxCapacity || newMaxLoad2 > Truck.maxCapacity) // new route is not feasible, revert changes
            {
                day1.route.orders.RemoveAt(index1);
                day1.route.orders.Insert(index1, order1);
                day2.route.orders.RemoveAt(index2);
                day2.route.orders.Insert(index2, order2);
            } 
        }

        // Randomly return whether an operator should be executed  
        public bool ExecuteOperator(float oldScore, float newScore)
        {
            if (newScore < oldScore)
            {
                return true;
            }

            int r = random.Next(0, 100);

            float p = (float) Math.Exp((oldScore - newScore) / Program.temperature);

            if (r < 100 * p)
            {
                return true;
            }

            return false;
        }


    }
}
