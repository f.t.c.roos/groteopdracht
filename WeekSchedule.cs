﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace GroteOpdracht
{
    class WeekSchedule
    {
        public DaySchedule[] daySchedules;
        public int totalTime;
        public int penalty;
        public int totalScore; // totalScore = totalTime + penaltyTime
        public List<Order> declinedOrders;

        public WeekSchedule()
        {
            this.daySchedules = new DaySchedule[10];
            this.totalTime = 0;
            this.penalty = 0;
            this.totalScore = 0;
            this.declinedOrders = new List<Order>();
        }

        // Remove orders that are planned in an infeasible manner (referred to as useless orders)
        public void RemoveUselessOrders()
        {
            List<Order> allOrdersIncDups = WeekOrders();
            List<(DaySchedule, Order)> ordersToBeRemoved = new List<(DaySchedule, Order)>();

            foreach (DaySchedule day in daySchedules)
            {
                List<Order> ordersofTheDay = day.route.orders; // make a copy to prevent modifying the collection during the for-loop

                foreach (Order order in ordersofTheDay)
                {
                    if ((allOrdersIncDups.FindAll(o => o.frequency == 2 && o == order).Count() == 1)
                    ||  (allOrdersIncDups.FindAll(o => o.frequency == 4 && o == order).Count() <= 3 && allOrdersIncDups.FindAll(o => o.frequency == 4 && o == order).Count() >= 1)
                    ||  (allOrdersIncDups.FindAll(o => o.frequency == 3 && o == order).Count() <= 2 && allOrdersIncDups.FindAll(o => o.frequency == 3 && o == order).Count() >= 1))
                    {
                        ordersToBeRemoved.Add((day, order));
                    }
                }
            }

            // Remove the useless orders. We cannot modify a collection in a for-loop, this is why we have to add a new for-loop
            foreach ((DaySchedule, Order) dayOrderPair in ordersToBeRemoved)
            {
                //this.daySchedules[dayOrderPair.Item1.dayNumber - 1].route.Remove(dayOrderPair.Item2);
                dayOrderPair.Item1.route.Remove(dayOrderPair.Item2);
                //declinedOrders.Add(dayOrderPair.Item2);
            }
        }

        // Returns a list that contains all orders (including duplicates) of a whole week
        public List<Order> WeekOrders()
        {
            List<Order> result = new List<Order>();
            
            foreach (DaySchedule day in daySchedules)
            {
                foreach (Order order in day.route.orders)
                {
                    result.Add(order);
                }
            }

            return result;
        }

        // Returns the total time of all routes combined
        public float Time()
        {
            float totalTime = 0;

            foreach (DaySchedule daySchedule in daySchedules)
            {
                totalTime += daySchedule.route.Time();
            }

            return totalTime;
        }

        // Returns the total penalty of a schedule
        public float Penalty()
        {
            float totalPenalty = 0;

            foreach (Order order in declinedOrders)
            {
                    totalPenalty += order.frequency * order.emptyingTime * 3f;
            }

            return totalPenalty;
        }

        // Returns the total score of a schedule
        public float Score()
        {
            return this.Time() + this.Penalty();
        }

        // Returns a list of all declined orders
        public void Declined()
        {
            List<Order> declinedOrders = new List<Order>();

            foreach (Order order in Program.orderList)
            {
                if (!Truck.allOrderIDs.Contains(order.orderID))
                {
                    declinedOrders.Add(order);
                }
            }

            this.declinedOrders = declinedOrders;
        }
    }
}
