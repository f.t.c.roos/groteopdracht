﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GroteOpdracht
{
    class Truck
    {
        public int truckNumber; // 1 of 2
        public int dayNumber; // (1, 2, 3, 4, 5)
        public int matrixID;
        public int stored; 
        public const int maxCapacity = 100000;
        public float timePassed; // seconden
        public const float maxTime = 43200; // seconden
        public const float dumpTime = 1800; // seconden
        public HashSet<Order> visitedOrders;
        public int visitedDumpCount;
        public static HashSet<int> allOrderIDs; // shared variable, houdt alle ingeplande orders over alle dagen bij

        public DaySchedule day;


        public Truck(int truckNumber, int dayNumber)
        {
            this.truckNumber = truckNumber;
            this.dayNumber = dayNumber;
            this.matrixID = 287; 
            this.stored = 0;
            this.timePassed = 0;
            this.visitedOrders = new HashSet<Order>();
            this.visitedDumpCount = 0;
            this.day = new DaySchedule(dayNumber, truckNumber);


            if (allOrderIDs == null)
            {
                allOrderIDs = new HashSet<int>();
            }
        }

        //Deze method vindt de dichstbijzijnde order die nog niet is voldaan en die gedaan kan worden in deze ronde met de truck.
        public Order GetNearestValidOrder() 
        {
            int leastDrivingTime = int.MaxValue;
            int drivingTimeToOrder;
            Order best_order = null;
            foreach (Order order in Program.orderList)
            {
                if (day.IsOrderAllowed(order)) // De order moet nog niet voltooid zijn en er moet rekening worden gehouden met de frequency
                {
                    drivingTimeToOrder = Program.afstandenMatrix[matrixID, order.matrixID];

                    if (drivingTimeToOrder <= leastDrivingTime)
                    {
                        best_order = order;
                        leastDrivingTime = drivingTimeToOrder;
                    }
                }
            }

            //if (best_order == null)
            //{
            //    throw new Exception("Could not get a valid order");
            //}

            return best_order;
        }

        public void PickUpGarbage(Order order)
        {
            float drivingTimeToOrder = (float) (Program.afstandenMatrix[matrixID, order.matrixID]);

            timePassed += drivingTimeToOrder + order.emptyingTime;
            stored += order.totalVolume;
            matrixID = order.matrixID;
            visitedOrders.Add(order);
            order.timesVisited++;
            if (order.timesVisited == order.frequency) // the order is fully completed
            {
                allOrderIDs.Add(order.orderID);
            }

            if (timePassed > maxTime)
            {
                throw new Exception("Exceeded truck's maxTime");
            }
            
            if (stored > maxCapacity)
            {
                throw new Exception("Exceeded truck's maxCapacity");
            }

        }

        public void DumpGarbage()
        {
            float drivingTimeToDump = Program.afstandenMatrix[matrixID, Program.dump.matrixID];

            timePassed += drivingTimeToDump + dumpTime;
            stored = 0;
            matrixID = Program.dump.matrixID;
        }

        public bool EnoughTimeToReturn(Order order)
        {
            float orderTime = (float) (Program.afstandenMatrix[matrixID, order.matrixID]) + order.emptyingTime;
            float dumpOrderTime = (float) (Program.afstandenMatrix[order.matrixID, Program.dump.matrixID]) + dumpTime; // het duurt 30 minuten om te storten
            return timePassed + orderTime + dumpOrderTime <= maxTime;
        }

        public bool EnoughSpace(Order order)
        {
            return stored + order.totalVolume <= maxCapacity;
        }

        public void GreedyHandleOrder()
        {
            timePassed = 0;
            stored = 0;

            while (true)
            {
                Order nextOrder = GetNearestValidOrder();

                if (nextOrder == null) // geen orders meer over: einde route
                {
                    if (stored > 0)
                    {
                        DumpGarbage();
                        day.route.Add(Program.dump);
                        visitedDumpCount++;
                    }
                    break;
                }

                else if (EnoughTimeToReturn(nextOrder) && EnoughSpace(nextOrder)) // voeg order toe
                {
                    PickUpGarbage(nextOrder);
                    day.route.Add(nextOrder);
                }
                else if (!EnoughTimeToReturn(nextOrder)) // er is geen tijd meer over: einde route
                {
                    if (stored > 0)
                    {
                        DumpGarbage();
                        day.route.Add(Program.dump);
                        visitedDumpCount++;
                    }
                    break;
                }
                else // we maken slechts een tussenstop
                {
                    DumpGarbage();
                    day.route.Add(Program.dump);
                    visitedDumpCount++;

                    if (visitedDumpCount >= 2) // we gaan niet meer dan 2 loops rijden 
                    {
                        break;
                    }
                }
            }
        }

        public int OrderCount()
        {
            return visitedOrders.Count;
        }

    }
}
