﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GroteOpdracht
{
    class DaySchedule
    {
        public int dayNumber;
        public int truckNumber;
        public Route route;

        public DaySchedule (int dayNumber, int truckNumber)
        {
            this.dayNumber = dayNumber;
            this.truckNumber = truckNumber;
            this.route = new Route(truckNumber, dayNumber);
        }


        // Checks if an order is allowed on a specific day
        public bool IsOrderAllowed(Order order)
        {
            switch (order.frequency)
            {
                case 1: // elke dag toegestaan
                    return order.NeverCompleted();
                case 2: // maandag-donderdag of dinsdag-vrijdag toegestaan
                    if (dayNumber == 1) return order.CompletedOnlyOn(4) || order.NeverCompleted();
                    if (dayNumber == 2) return order.CompletedOnlyOn(5) || order.NeverCompleted();
                    if (dayNumber == 3) return false;
                    if (dayNumber == 4) return order.CompletedOnlyOn(1) || order.NeverCompleted();
                    if (dayNumber == 5) return order.CompletedOnlyOn(2) || order.NeverCompleted();
                    break;
                case 3: // maandag-woensdag-vrijdag toegestaan
                    if (dayNumber == 1) return order.CompletedOnlyOn(3) || order.CompletedOnlyOn(5) || order.CompletedOnlyOn(3, 5) || order.NeverCompleted();
                    if (dayNumber == 2) return false;
                    if (dayNumber == 3) return order.CompletedOnlyOn(1) || order.CompletedOnlyOn(5) || order.CompletedOnlyOn(1, 5) || order.NeverCompleted();
                    if (dayNumber == 4) return false;
                    if (dayNumber == 5) return order.CompletedOnlyOn(1) || order.CompletedOnlyOn(3) || order.CompletedOnlyOn(1, 3) || order.NeverCompleted();
                    break;
                case 4: // every combination allowed
                    return order.CompletedThreeOrLessTimes(dayNumber);
            }

            return false;
        }

    }
}
