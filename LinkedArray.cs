﻿//using System;
//using System.Collections.Generic;
//using System.Text;

//namespace GroteOpdracht
//{
//    struct Node
//    {
//        // pointers
//        public int previous;
//        public int next;

//        // Node data
//        public int vrachtauto;  // 1 of 2
//        public int dagnummer;   // 1 .. 5
//        public int dagteller;   // Hoeveelste adres dat het voertuig op die dag aandoet(begin met 1, 2, …)
//        public int orderID;
//        public int matrixID;
//        public double emptyingTime;
//        public int frequency;
//        public int volume;

//        public Node(int previous, int next, int vrachtauto, int dagnummer, int dagteller, int orderID, int matrixID, double emptyingTime, int frequency, int volume)
//        {
//            this.previous = previous;
//            this.next = next;
//            this.vrachtauto = vrachtauto;
//            this.dagnummer = dagnummer;
//            this.dagteller = dagteller;
//            this.orderID = orderID;
//            this.matrixID = matrixID;
//            this.emptyingTime = emptyingTime;
//            this.frequency = frequency;
//            this.volume = volume;
//        }
//    }

//    // Datastructuur is custom LinkedArray omdat een array de snelste datastructuur is en je met de pointers makkelijk iets kan tussenvoegen.
//    // Als Node hebben we een struct gemaakt omdat de Node niet veel wordt bewerkt en een Struct efficienter is in geheugen
//    // De Node heeft geen previous pointer omdat dat extra werk zou zijn terwijl die pointer alleen nodig is voor een snelle AddBefore functie
//    class LinkedArray
//    {
//        public Node[] route;
//        public int head = 0;
//        public int tail = 0;
//        public int lengthArray;
//        public int Count = 0;

//        public double totalTime; // totale tijd van alle routes
//        public double penalty; // totale penaltykosten

//        // 1=add, 2=move, 3=swap, 4=remove
//        public int lastOperation = 0;
//        // add
//        public int randomIndexForNotCompleted;
//        public int randomIndexForLinkedArray;
//        // move
//        public int randomIndexForLinkedArrayMoveFrom;
//        public int randomIndexForLinkedArrayMoveTo;
//        // swap
//        public int randomIndexForLinkedArraySwapOne;
//        public int randomIndexForLinkedArraySwapTwo;
//        // Remove
//        public int randomIndexForRemove;

//        public Random random = new Random();

//        // De LinkedArray heeft een length en een count, dat is verwarrend. 
//        // De Length is afkomstig uit de sum van de frequencies uit Orderbestand.txt zodat de array gemaakt kan worden. Die kan net zo goed stiekem constant zijn
//        // De count gaat over de lengte van de LinkedArray die aan het begin 0 is
//        public LinkedArray(int length, double beginStrafTijd)
//        {
//            // de sum van de frequencies keer twee voor extra ruimte in de LinkedArray
//            this.lengthArray = length * 2;
//            this.route = new Node[length * 2];

//            this.totalTime = 0;
//            this.penalty = beginStrafTijd;
//        }

//        // PrintAscending gaat door de linked list door de pointer te volgen 
//        public void PrintAscending()
//        {
//            int current = head;
//            for (int i = 0; i < Count - 2; i++)
//            {
//                Console.WriteLine(
//                    this.route[current].vrachtauto +
//                    "; " +
//                    this.route[current].dagnummer +
//                    "; " +
//                    this.route[current].dagteller +
//                    "; " +
//                    this.route[current].orderID
//                );
//                if (route[current].next != -1)
//                {
//                    current = route[current].next;
//                }
//            }
//        }

//        // PrintLinear loopt linair door de array en print de stops in niet oplopende dagnummer/dagteller en met verwijderde nodes
//        public void Printlinear()
//        {
//            for (int i = 0; i < Count; i++)
//            {
//                Console.WriteLine(
//                    this.route[i].vrachtauto.ToString() +
//                    "; " +
//                    this.route[i].dagnummer.ToString() +
//                    "; " +
//                    this.route[i].dagteller.ToString() +
//                    "; " +
//                    this.route[i].orderID.ToString()
//                );
//            }
//        }

//        public void Execute()
//        {
//            if (lastOperation == 1)
//            {
//                AddOrderStopAfterNode(Program.notCompleted[randomIndexForNotCompleted], route[randomIndexForLinkedArray]);
//            }
//            else if (lastOperation == 2)
//            {
//                Move(randomIndexForLinkedArrayMoveFrom, randomIndexForLinkedArrayMoveTo);
//            }
//            else if (lastOperation == 3)
//            {
//                Swap(route[randomIndexForLinkedArraySwapOne], route[randomIndexForLinkedArraySwapTwo]);
//            }
//            else if (lastOperation == 4)
//            {
//                Remove(route[randomIndexForRemove]);
//            }
//            else
//            {
//                throw new Exception();
//            }
//        }

//        public double ComputeRandomNext()
//        {
//            // Add, Move, Swap, Remove, 
//            int prob = random.Next(100);

//            if (prob < 25 && Program.notCompleted.Count > 0) // add
//            {
//                // pak een random not completed order
//                randomIndexForNotCompleted = random.Next(Program.notCompleted.Count);
//                randomIndexForLinkedArray = random.Next(this.Count);
//                lastOperation = 1;

//                return ComputeAdd(Program.notCompleted[randomIndexForNotCompleted], randomIndexForLinkedArray);
//            }
//            else if (prob < 50) // move
//            {
//                // 
//                randomIndexForLinkedArrayMoveFrom = random.Next(this.Count);
//                randomIndexForLinkedArrayMoveTo = random.Next(this.Count);
//                while (randomIndexForLinkedArrayMoveFrom == randomIndexForLinkedArrayMoveTo)
//                {
//                    randomIndexForLinkedArrayMoveTo = random.Next(this.Count);
//                }
//                lastOperation = 2;

//                return ComputeMove(randomIndexForLinkedArrayMoveFrom, randomIndexForLinkedArrayMoveTo);
//            }
//            else if (prob < 75) // swap
//            {
//                //
//                randomIndexForLinkedArraySwapOne = random.Next(this.Count);
//                randomIndexForLinkedArraySwapTwo = random.Next(this.Count);
//                while (randomIndexForLinkedArraySwapOne == randomIndexForLinkedArraySwapTwo)
//                {
//                    randomIndexForLinkedArraySwapOne = random.Next(this.Count);
//                }
//                lastOperation = 3;

//                return ComputeSwap(randomIndexForLinkedArraySwapOne, randomIndexForLinkedArraySwapTwo);
//            }
//            else // remove
//            {
//                //
//                randomIndexForRemove = random.Next(this.Count);
//                lastOperation = 4;

//                return ComputeRemove(randomIndexForRemove);
//            }
//        }

//        // AddOrderStopAfterNode
//        public double ComputeAdd(Order order, int indexVoorInvoegen)
//        {
//            double testTotalTime = this.totalTime;

//            if (route[indexVoorInvoegen].previous == -2 || route[indexVoorInvoegen].next == -2)
//            {
//                throw new Exception();
//            }
//            else
//            {
//                if (indexVoorInvoegen != tail)
//                {
//                    testTotalTime -= Program.afstandenMatrix[route[indexVoorInvoegen].matrixID, route[route[indexVoorInvoegen].next].matrixID].Item2;
//                    testTotalTime += Program.afstandenMatrix[route[indexVoorInvoegen].matrixID, order.matrixID].Item2
//                        + Program.afstandenMatrix[order.matrixID, route[route[indexVoorInvoegen].next].matrixID].Item2;
//                }
//                else
//                {
//                    testTotalTime += Program.afstandenMatrix[route[indexVoorInvoegen].matrixID, order.matrixID].Item2;
//                }

//                if (order.orderID == 0)
//                {
//                    testTotalTime += Program.stortLeegTijd;
//                }
//                else
//                {
//                    testTotalTime += order.emptyingTime;
//                }

//                if (order.aantalKeerBezocht == 1)
//                {
//                    penalty -= order.frequency * order.emptyingTime * 3;
//                }

//                if (route[indexVoorInvoegen].orderID == 0)
//                {

//                }
//                else if (ValidCapacityCheck(route[indexVoorInvoegen].vrachtauto, route[indexVoorInvoegen].dagnummer))
//                {
//                    testTotalTime += 1000;
//                }

//                return testTotalTime;
//            }

//        }

//        // true means valid, false means invalid thus penalty 
//        public bool ValidCapacityCheck(int vrachtautoID, int dagnummer)
//        {
//            Node node = route[head];
//            while (node.vrachtauto != vrachtautoID && node.dagnummer != dagnummer)
//            {
//                node = route[node.next];
//            }

//            int load = 0;
//            while (node.vrachtauto == vrachtautoID && node.dagnummer == dagnummer && node.orderID != 0 & node.next != -1)
//            {
//                load += node.volume;

//                if (load > Program.maxCapacity)
//                {
//                    return false;
//                }

//                node = route[node.next];
//            }

//            return true;
//        }
//        public double ComputeMove(int indexFrom, int indexTo)
//        {
//            double testTotalTime = this.totalTime;


//            return testTotalTime;
//        }
//        public double ComputeSwap(int indexOne, int indexTwo)
//        {
//            double testTotalTime = this.totalTime;

//            return testTotalTime;
//        }
//        public double ComputeRemove(int indexLA)
//        {
//            double testTotalTime = this.totalTime;



//            return testTotalTime;
//        }

//        public void Move(int indexfrom, int indexto)
//        {
//            AddNodeAfterNode(route[indexfrom], route[indexto]);

//            Remove(route[indexfrom]);
//        }

//        // voegt een nieuwe node toe aan het einde van de linked array die nergens mee verbonden is en de nieuwe tail is
//        public void AddLast(Truck stop, Order order)
//        {
//            Node add = new Node(tail, -1, stop.truckID, stop.day, stop.truckCount, order.orderID, order.matrixID, order.emptyingTime, order.frequency, order.totalVolume);

//            if (this.Count != 0)
//            {
//                this.route[tail].next = this.Count;
//            }

//            if (this.Count > 0)
//            {
//                this.totalTime += Program.afstandenMatrix[route[tail].matrixID, order.matrixID].Item2;
//            }
//            else
//            {
//                this.totalTime += Program.afstandenMatrix[Program.dump.matrixID, order.matrixID].Item2;
//            }

//            if (order.orderID == 0)
//            {
//                this.totalTime += Program.stortLeegTijd;
//            }
//            else
//            {
//                //throw new Exception("laatste order moet dump zijn");
//                this.totalTime += order.emptyingTime;
//            }

//            this.route[this.Count] = add;
//            this.tail = this.Count;
//            this.Count++;
//        }

//        // voegt een nieuwe node toe aan de head van de linked array die verbonden is met de vorige head en de nieuwe head is
//        public void AddFirst(Truck stop, Order order)
//        {
//            Node add = new Node(-1, head, stop.truckID, stop.day, stop.truckCount, order.orderID, order.matrixID, order.emptyingTime, order.frequency, order.totalVolume); // order.emptyingTime == 0
//            if (this.Count != 0)
//            {
//                this.route[this.head].previous = this.Count;
//            }

//            if (this.Count > 0)
//            {
//                this.totalTime -= Program.afstandenMatrix[Program.dump.matrixID, route[head].matrixID].Item2;
//                this.totalTime += Program.afstandenMatrix[Program.dump.matrixID, order.matrixID].Item2 +
//                    Program.afstandenMatrix[order.matrixID, route[head].matrixID].Item2;
//            }
//            else
//            {
//                this.totalTime += Program.afstandenMatrix[Program.dump.matrixID, order.matrixID].Item2;
//            }

//            if (order.orderID == 0)
//            {
//                this.totalTime += Program.stortLeegTijd;
//            }
//            else
//            {
//                this.totalTime += order.emptyingTime;
//            }

//            this.route[this.Count] = add;
//            this.head = this.Count;
//            this.Count++;
//        }

//        public void AddNodeAfterNode(Node add, Node existingStop)
//        {
//            if (existingStop.previous == -2 || existingStop.next == -2)
//            {
//                throw new Exception();
//            }
//            else
//            {
//                this.route[this.Count] = add;

//                // capture case that existingStop is tail
//                if (existingStop.next != -1)
//                {
//                    route[existingStop.next].previous = this.Count;

//                    totalTime -= Program.afstandenMatrix[existingStop.matrixID, route[existingStop.next].matrixID].Item2;
//                    totalTime += Program.afstandenMatrix[existingStop.matrixID, add.matrixID].Item2 + Program.afstandenMatrix[add.matrixID, route[existingStop.next].matrixID].Item2;
//                }
//                else
//                {
//                    totalTime += Program.afstandenMatrix[existingStop.matrixID, add.matrixID].Item2;
//                }

//                existingStop.next = this.Count;
//                this.Count++;

//                if (add.orderID == 0)
//                {
//                    this.totalTime += Program.stortLeegTijd;
//                }
//                else
//                {
//                    this.totalTime += add.emptyingTime;
//                }
//            }
//        }

//        public void AddOrderStopAfterNode(Order order, Node existingStop)
//        {
//            if (existingStop.previous == -2 || existingStop.next == -2)
//            {
//                throw new Exception();
//            }
//            else
//            {
//                Node add = new Node(route[existingStop.previous].next, existingStop.next, existingStop.vrachtauto, existingStop.dagnummer,
//                    existingStop.dagteller + 1, order.orderID, order.matrixID, order.emptyingTime, order.frequency, order.totalVolume);

//                this.route[this.Count] = add;

//                // capture case that existingStop is tail
//                if (existingStop.next != -1)
//                {
//                    route[existingStop.next].previous = this.Count;

//                    totalTime -= Program.afstandenMatrix[existingStop.matrixID, route[existingStop.next].matrixID].Item2;
//                    totalTime += Program.afstandenMatrix[existingStop.matrixID, order.matrixID].Item2 + Program.afstandenMatrix[order.matrixID, route[existingStop.next].matrixID].Item2;
//                }
//                else
//                {
//                    totalTime += Program.afstandenMatrix[existingStop.matrixID, order.matrixID].Item2;
//                }

//                existingStop.next = this.Count;
//                this.Count++;

//                if (order.orderID == 0)
//                {
//                    this.totalTime += Program.stortLeegTijd;
//                }
//                else
//                {
//                    this.totalTime += order.emptyingTime;
//                }

//                Program.notCompleted.Remove(order);
//            }
//        }

//        // voegt een nieuwe node in na een bestaande node
//        public void AddOrderStopAfterNode(Truck stop, Order order, Node existingStop)
//        {
//            if (existingStop.previous == -2 || existingStop.next == -2)
//            {
//                throw new Exception();
//            }
//            else
//            {
//                Node add = new Node(route[existingStop.previous].next, existingStop.next, stop.truckID, stop.day, stop.truckCount, order.orderID, order.matrixID, order.emptyingTime, order.frequency, order.totalVolume);

//                this.route[this.Count] = add;

//                // capture case that existingStop is tail
//                if (existingStop.next != -1)
//                {
//                    route[existingStop.next].previous = this.Count;

//                    totalTime -= Program.afstandenMatrix[existingStop.matrixID, route[existingStop.next].matrixID].Item2;
//                    totalTime += Program.afstandenMatrix[existingStop.matrixID, order.matrixID].Item2 + Program.afstandenMatrix[order.matrixID, route[existingStop.next].matrixID].Item2;
//                }
//                else
//                {
//                    totalTime += Program.afstandenMatrix[existingStop.matrixID, order.matrixID].Item2;
//                }

//                existingStop.next = this.Count;
//                this.Count++;

//                if (order.orderID == 0)
//                {
//                    this.totalTime += Program.stortLeegTijd;
//                }
//                else
//                {
//                    this.totalTime += order.emptyingTime;
//                }

//                Program.notCompleted.Remove(order);
//            }
//        }

//        public void AddStopBeforeNode(Truck stop, Order order, Node existingStop)
//        {
//            if (existingStop.previous == -2 || existingStop.next == -2)
//            {
//                throw new Exception();
//            }
//            else
//            {
//                Node add = new Node(existingStop.previous, route[existingStop.previous].next, stop.truckID, stop.day, stop.truckCount, order.orderID, order.matrixID, order.emptyingTime, order.frequency, order.totalVolume);
//                this.route[this.Count] = add;

//                // capture case that existingStop is head
//                if (existingStop.previous != -1)
//                {
//                    route[existingStop.previous].next = this.Count;

//                    totalTime -= Program.afstandenMatrix[Program.dump.matrixID, existingStop.matrixID].Item2;
//                    totalTime += Program.afstandenMatrix[Program.dump.matrixID, order.matrixID].Item2 + Program.afstandenMatrix[order.matrixID, existingStop.matrixID].Item2;
//                }
//                else
//                {
//                    totalTime -= Program.afstandenMatrix[route[existingStop.previous].matrixID, existingStop.matrixID].Item2;
//                    totalTime += Program.afstandenMatrix[route[existingStop.previous].matrixID, order.matrixID].Item2 + Program.afstandenMatrix[order.matrixID, existingStop.matrixID].Item2;

//                }

//                if (order.orderID == 0)
//                {
//                    this.totalTime += Program.stortLeegTijd;
//                }
//                else
//                {
//                    this.totalTime += order.emptyingTime;
//                }

//                existingStop.previous = this.Count;
//                this.Count++;
//            }
//        }
//        public void Clear()
//        {
//            this.route = new Node[lengthArray];
//            this.Count = 0;
//        }
//        public bool Contains(Node zoek)
//        {
//            Node vergelijk = route[this.head];
//            for (int i = 0; i < Count; i++)
//            {
//                if (zoek.orderID == vergelijk.orderID)
//                {
//                    return true;
//                }
//                else
//                {
//                    vergelijk = route[vergelijk.next];
//                }
//            }
//            return false;
//        }

//        public Node[] FindAll(Order order)
//        {
//            Node[] voorkomens = new Node[5];
//            int head = 0;

//            Node vergelijk = route[this.head];
//            for (int i = 0; i < Count - 1; i++)
//            {
//                if (order.orderID == vergelijk.orderID)
//                {
//                    voorkomens[head] = vergelijk;
//                    head++;
//                }

//                if (vergelijk.next != -1)
//                {
//                    vergelijk = route[vergelijk.next];
//                }
//                else
//                {
//                    break;
//                }
//            }
//            return voorkomens;
//        }

//        public void Remove(Node stop)
//        {
//            if (stop.orderID == 0)
//            {
//                this.totalTime -= Program.stortLeegTijd;
//            }
//            else
//            {
//                this.totalTime -= stop.emptyingTime;
//            }

//            if (this.Count == 0)
//            {
//                throw new ArgumentOutOfRangeException();
//            }
//            else if (this.Count == 1)
//            {
//                this.totalTime -= Program.afstandenMatrix[Program.dump.matrixID, stop.matrixID].Item2;

//                this.tail = -1;
//                this.head = -1;
//            }
//            else if (stop.next == -1)
//            {
//                this.totalTime -= Program.afstandenMatrix[route[stop.previous].matrixID, stop.matrixID].Item2;

//                this.tail = stop.previous;
//                this.route[this.tail].next = -1;
//            }
//            else if (stop.previous == -1)
//            {
//                this.totalTime -= Program.afstandenMatrix[Program.dump.matrixID, stop.matrixID].Item2
//                    + Program.afstandenMatrix[stop.matrixID, route[stop.next].matrixID].Item2;
//                this.totalTime += Program.afstandenMatrix[Program.dump.matrixID, route[stop.next].matrixID].Item2;

//                this.head = stop.next;
//                this.route[this.head].previous = -1;
//            }
//            else
//            {
//                // x -= y <=> x = x - y  MAAR x -= y - z <=> x = x - y + z
//                this.totalTime -= Program.afstandenMatrix[route[stop.previous].matrixID, stop.matrixID].Item2
//                    + Program.afstandenMatrix[stop.matrixID, route[stop.next].matrixID].Item2;
//                this.totalTime += Program.afstandenMatrix[route[stop.previous].matrixID, route[stop.next].matrixID].Item2;

//                this.route[stop.previous].next = stop.next;
//                this.route[stop.next].previous = stop.previous;

//                // dagteller van truck goed verbinden
//                int pointer = stop.next;
//                while (this.route[pointer].next != -1)
//                {
//                    if (this.route[pointer].vrachtauto == stop.vrachtauto && this.route[pointer].dagnummer == stop.dagnummer)
//                    {
//                        this.route[pointer].dagteller -= 1;
//                    }
//                    pointer = this.route[pointer].next;
//                }
//                if (this.route[pointer].vrachtauto == stop.vrachtauto && this.route[pointer].dagnummer == stop.dagnummer && this.route[pointer].next == -1)
//                {
//                    this.route[tail].dagteller -= 1;
//                }
//            }

//            Order teruggeven;
//            foreach (Order order in Program.notCompleted)
//            {
//                if (stop.orderID == order.orderID)
//                {
//                    order.aantalKeerBezocht--;
//                    break;
//                }
//                else
//                {
//                    teruggeven = new Order(stop.orderID, stop.frequency, stop.volume, stop.emptyingTime, stop.matrixID, order.aantalKeerBezocht);
//                    Program.notCompleted.Add(teruggeven);
//                    break;
//                }
//            }

//            // de pointes van de node resetten: -1 is in gebruik door de eerste en laaste node in de LinkedArray, dus ik gebruk -2
//            stop.previous = -2;
//            stop.next = -2;
//        }
//        public void RemoveForUselessNodes(Node stop)
//        {
//            if (stop.orderID == 0)
//            {
//                this.totalTime -= Program.stortLeegTijd;
//            }
//            else
//            {
//                this.totalTime -= stop.emptyingTime;
//            }

//            if (this.Count == 0)
//            {
//                throw new ArgumentOutOfRangeException();
//            }
//            else if (this.Count == 1)
//            {
//                this.totalTime -= Program.afstandenMatrix[Program.dump.matrixID, stop.matrixID].Item2;

//                this.tail = -1;
//                this.head = -1;
//            }
//            else if (stop.next == -1)
//            {
//                this.totalTime -= Program.afstandenMatrix[route[stop.previous].matrixID, stop.matrixID].Item2;

//                this.tail = stop.previous;
//                this.route[this.tail].next = -1;
//            }
//            else if (stop.previous == -1)
//            {
//                this.totalTime -= Program.afstandenMatrix[Program.dump.matrixID, stop.matrixID].Item2
//                    + Program.afstandenMatrix[stop.matrixID, route[stop.next].matrixID].Item2;
//                this.totalTime += Program.afstandenMatrix[Program.dump.matrixID, route[stop.next].matrixID].Item2;

//                this.head = stop.next;
//                this.route[this.head].previous = -1;
//            }
//            else
//            {
//                // x -= y <=> x = x - y  MAAR x -= y - z <=> x = x - y + z
//                this.totalTime -= Program.afstandenMatrix[route[stop.previous].matrixID, stop.matrixID].Item2
//                    + Program.afstandenMatrix[stop.matrixID, route[stop.next].matrixID].Item2;
//                this.totalTime += Program.afstandenMatrix[route[stop.previous].matrixID, route[stop.next].matrixID].Item2;

//                this.route[stop.previous].next = stop.next;
//                this.route[stop.next].previous = stop.previous;

//                // dagteller van truck goed verbinden
//                int pointer = stop.next;
//                while (this.route[pointer].next != -1)
//                {
//                    if (this.route[pointer].vrachtauto == stop.vrachtauto && this.route[pointer].dagnummer == stop.dagnummer)
//                    {
//                        this.route[pointer].dagteller -= 1;
//                    }
//                    pointer = this.route[pointer].next;
//                }
//                if (this.route[pointer].vrachtauto == stop.vrachtauto && this.route[pointer].dagnummer == stop.dagnummer && this.route[pointer].next == -1)
//                {
//                    this.route[tail].dagteller -= 1;
//                }
//            }

//            // de pointes van de node resetten: -1 is in gebruik door de eerste en laaste node in de LinkedArray, dus ik gebruk -2
//            stop.previous = -2;
//            stop.next = -2;
//        }
//        public void RemoveFirst()
//        {
//            if (route[head].orderID == 0)
//            {
//                // zou niet mogen
//                this.totalTime -= Program.stortLeegTijd;
//            }
//            else
//            {
//                this.totalTime -= route[head].emptyingTime;
//            }

//            this.totalTime -= Program.afstandenMatrix[Program.dump.matrixID, route[head].matrixID].Item2
//                - Program.afstandenMatrix[route[head].matrixID, route[route[head].next].matrixID].Item2;
//            this.totalTime += Program.afstandenMatrix[Program.dump.matrixID, route[route[head].next].matrixID].Item2;

//            if (this.Count == 0)
//            {
//                throw new ArgumentOutOfRangeException();
//            }
//            else if (this.Count == 1)
//            {
//                this.route[this.head].previous = -2;
//                this.route[this.head].next = -2;

//                this.tail = -1;
//                this.head = -1;
//            }
//            else
//            {
//                int remove = this.head;

//                this.head = this.route[head].next;
//                this.route[this.head].previous = -1;

//                this.route[remove].previous = -2;
//                this.route[remove].next = -2;
//            }

//            this.Count--;
//        }
//        public void RemoveLast()
//        {
//            if (route[tail].orderID == 0)
//            {
//                this.totalTime -= Program.stortLeegTijd;
//            }
//            else
//            {
//                this.totalTime -= route[head].emptyingTime;
//            }

//            this.totalTime -= Program.afstandenMatrix[route[route[tail].previous].matrixID, route[tail].matrixID].Item2;

//            if (this.Count == 0)
//            {
//                throw new ArgumentOutOfRangeException();
//            }
//            else if (this.Count == 1)
//            {
//                this.route[this.head].previous = -2;
//                this.route[this.head].next = -2;

//                this.tail = -1;
//                this.head = -1;
//            }
//            else
//            {
//                int remove = this.head;

//                this.tail = this.route[this.tail].next;
//                this.route[this.tail].next = -1;

//                this.route[remove].previous = -2;
//                this.route[remove].next = -2;
//            }

//            this.Count--;
//        }
//        public void Swap(Node one, Node two)
//        {
//            // orderID;
//            int swap = one.orderID;
//            one.orderID = two.orderID;
//            two.orderID = swap;

//            // tijd 
//            if (Count < 3)
//            {
//                throw new Exception();
//            }
//            else if (one.next == -1 && two.previous != -1)
//            {
//                totalTime -= Program.afstandenMatrix[two.matrixID, route[two.next].matrixID].Item2;
//                totalTime -= Program.afstandenMatrix[route[two.previous].matrixID, two.matrixID].Item2;

//                totalTime -= Program.afstandenMatrix[route[one.previous].matrixID, one.matrixID].Item2;

//                totalTime += Program.afstandenMatrix[one.matrixID, route[two.next].matrixID].Item2;
//                totalTime += Program.afstandenMatrix[route[two.previous].matrixID, one.matrixID].Item2;

//                totalTime += Program.afstandenMatrix[route[one.previous].matrixID, two.matrixID].Item2;
//            }
//            else if (two.next == -1 && one.previous != -1)
//            {
//                totalTime -= Program.afstandenMatrix[one.matrixID, route[one.next].matrixID].Item2;
//                totalTime -= Program.afstandenMatrix[route[one.previous].matrixID, one.matrixID].Item2;

//                totalTime -= Program.afstandenMatrix[route[two.previous].matrixID, two.matrixID].Item2;

//                totalTime += Program.afstandenMatrix[two.matrixID, route[one.next].matrixID].Item2;
//                totalTime += Program.afstandenMatrix[route[one.previous].matrixID, two.matrixID].Item2;

//                totalTime += Program.afstandenMatrix[route[two.previous].matrixID, one.matrixID].Item2;
//            }
//            else if (one.previous == -1 && two.next != -1)
//            {
//                totalTime -= Program.afstandenMatrix[two.matrixID, route[two.next].matrixID].Item2;
//                totalTime -= Program.afstandenMatrix[route[two.previous].matrixID, two.matrixID].Item2;

//                totalTime -= Program.afstandenMatrix[Program.dump.matrixID, one.matrixID].Item2;
//                totalTime -= Program.afstandenMatrix[one.matrixID, route[one.next].matrixID].Item2;

//                totalTime += Program.afstandenMatrix[one.matrixID, route[two.next].matrixID].Item2;
//                totalTime += Program.afstandenMatrix[route[two.previous].matrixID, one.matrixID].Item2;

//                totalTime += Program.afstandenMatrix[Program.dump.matrixID, two.matrixID].Item2;
//                totalTime += Program.afstandenMatrix[two.matrixID, route[one.next].matrixID].Item2;
//            }
//            else if (two.previous == -1 && one.next != -1)
//            {
//                totalTime -= Program.afstandenMatrix[one.matrixID, route[one.next].matrixID].Item2;
//                totalTime -= Program.afstandenMatrix[route[one.previous].matrixID, one.matrixID].Item2;

//                totalTime -= Program.afstandenMatrix[Program.dump.matrixID, two.matrixID].Item2;
//                totalTime -= Program.afstandenMatrix[two.matrixID, route[two.next].matrixID].Item2;

//                totalTime += Program.afstandenMatrix[two.matrixID, route[one.next].matrixID].Item2;
//                totalTime += Program.afstandenMatrix[route[one.previous].matrixID, two.matrixID].Item2;

//                totalTime += Program.afstandenMatrix[Program.dump.matrixID, one.matrixID].Item2;
//                totalTime += Program.afstandenMatrix[one.matrixID, route[two.next].matrixID].Item2;
//            }
//            else if (one.next == -1 && two.previous == -1)
//            {
//                totalTime -= Program.afstandenMatrix[two.matrixID, route[two.next].matrixID].Item2;
//                totalTime -= Program.afstandenMatrix[Program.dump.matrixID, two.matrixID].Item2;

//                totalTime -= Program.afstandenMatrix[route[one.previous].matrixID, one.matrixID].Item2;

//                totalTime += Program.afstandenMatrix[one.matrixID, route[two.next].matrixID].Item2;
//                totalTime += Program.afstandenMatrix[Program.dump.matrixID, one.matrixID].Item2;

//                totalTime += Program.afstandenMatrix[route[one.previous].matrixID, two.matrixID].Item2;
//            }
//            else if (two.next == -1 && one.previous == -1)
//            {
//                totalTime -= Program.afstandenMatrix[one.matrixID, route[one.next].matrixID].Item2;
//                totalTime -= Program.afstandenMatrix[Program.dump.matrixID, one.matrixID].Item2;

//                totalTime -= Program.afstandenMatrix[route[two.previous].matrixID, two.matrixID].Item2;

//                totalTime += Program.afstandenMatrix[two.matrixID, route[one.next].matrixID].Item2;
//                totalTime += Program.afstandenMatrix[Program.dump.matrixID, two.matrixID].Item2;

//                totalTime += Program.afstandenMatrix[route[two.previous].matrixID, one.matrixID].Item2;
//            }
//            else
//            {
//                totalTime -= Program.afstandenMatrix[one.matrixID, route[one.next].matrixID].Item2;
//                totalTime -= Program.afstandenMatrix[route[one.previous].matrixID, one.matrixID].Item2;

//                totalTime -= Program.afstandenMatrix[two.matrixID, route[two.next].matrixID].Item2;
//                totalTime -= Program.afstandenMatrix[route[two.previous].matrixID, two.matrixID].Item2;

//                totalTime += Program.afstandenMatrix[two.matrixID, route[one.next].matrixID].Item2;
//                totalTime += Program.afstandenMatrix[route[one.previous].matrixID, two.matrixID].Item2;

//                totalTime += Program.afstandenMatrix[one.matrixID, route[two.next].matrixID].Item2;
//                totalTime += Program.afstandenMatrix[route[two.previous].matrixID, one.matrixID].Item2;
//            }

//            // pointers
//            int pointerSwapPrevious = two.previous;
//            int pointerSwapNext = two.next;

//            two.previous = one.previous;
//            two.next = one.next;

//            one.previous = pointerSwapPrevious;
//            one.next = pointerSwapNext;

//            // vrachtautoID
//            int vrachtautoIDSwap = two.vrachtauto;
//            two.vrachtauto = one.vrachtauto;
//            one.vrachtauto = two.vrachtauto;

//            // dagnummer
//            int dagnummerSwap = two.dagnummer;
//            two.dagnummer = one.dagnummer;
//            one.dagnummer = dagnummerSwap;

//            // dagteller
//            int dagtellerSwap = two.dagteller;
//            two.dagteller = one.dagteller;
//            one.dagteller = dagtellerSwap;
//        }
//    }
//}

