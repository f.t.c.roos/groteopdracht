﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GroteOpdracht
{
    class Route
    {
        public List<Order> orders; // (index, order) houdt de orders bij van deze route
        public int truckNumber; // 1 of 2
        public int dayNumber; // 1 of 2 of 3 of 4 of 5

        public Route (int truckNumber, int dayNumber)
        {
            this.orders = new List<Order>();
            this.truckNumber = truckNumber;
            this.dayNumber = dayNumber;
        }

        // Prints the route in the correct format
        public void Print()
        {
            for (int i = 1; i <= orders.Count; i++)
            {
                Console.WriteLine($"{truckNumber};{dayNumber};{i};{orders[i - 1].orderID}");
            }
        }

        // Returns the total time it takes to process a route
        public float Time()
        {
            float totalTime = 0f;
            int matrixID = 287; // elke route start bij de dump 

            foreach (Order order in orders)
            {
                totalTime += Program.afstandenMatrix[matrixID, order.matrixID] + order.emptyingTime;
                matrixID = order.matrixID;
            }

            return totalTime;
        }


        // Add an order to the route
        public void Add(Order order)
        {
            orders.Add(order);

            if (order != Program.dump)
            {
                order.SetFlagTrue(dayNumber);
            }
        }

        // Add an order to the route at a specific index
        public void InsertAt(int index, Order order)
        {
            orders.Insert(index, order);

            if (order != Program.dump)
            {
                order.SetFlagTrue(dayNumber);
            }
        }

        // Remove an order from the route
        public void Remove(Order order)
        {
            orders.Remove(order);

            if (order != Program.dump)
            {
                order.SetFlagFalse(dayNumber);
            }
        }

        // Remove an order from the route at a specific index
        public void RemoveAt(int index)
        {
            Order order = orders[index];
            orders.RemoveAt(index);

            if (order != Program.dump)
            {
                order.SetFlagFalse(dayNumber);
            }
        }
        

        // Given an order, return the loop it is in (a loop ends whenever the dump is visited)
        public List<Order> Loop(Order order)
        {
            List<Order> loop = new List<Order>();

            foreach (Order o in orders)
            {
                if (o == Program.dump)  // we are visiting the dump 
                {
                    if (loop.Contains(order))
                    {
                        return loop;
                    }
                    else
                    {
                        loop.Clear();
                    }
                }
                else
                {
                    loop.Add(o);
                }                
            }

            throw new Exception("Order is not contained in this route (route.Loop)"); // fail-safe
        }

        // Given a loop, returns its maximum load
        public int MaxLoad(List<Order> loop)
        {
            int maxLoad = 0;

            foreach (Order order in loop)
            {
                maxLoad += order.totalVolume;
            }

            return maxLoad;
        }

    }
}
