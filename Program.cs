﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace GroteOpdracht
{
    class Program
    {
        public static HashSet<Order> orderList;
        public static int[,] afstandenMatrix;
        public static Order dump = new Order(287);
        public static WeekSchedule schedule;
        public const float alfa = 0.99f;
        public const float epsilon = 0.001f;

        public static SA sa = new SA();

        public static Random random = new Random();

        public static double temperature;

        static void Main(string[] args)
        {
            string[] linesOrder = System.IO.File.ReadAllLines("Orderbestand.txt");
            string[] linesAfstanden = System.IO.File.ReadAllLines("Afstanden.txt");

            orderList = new HashSet<Order>();
            afstandenMatrix = new int[1099, 1099];
            schedule = new WeekSchedule();

            FillOrderList(linesOrder);
            FillAfstandenMatrix(linesAfstanden);

            Truck truckA1 = new Truck(1, 1);
            Truck truckA2 = new Truck(1, 2);
            Truck truckA3 = new Truck(1, 3);
            Truck truckA4 = new Truck(1, 4);
            Truck truckA5 = new Truck(1, 5);
            Truck truckB1 = new Truck(2, 1);
            Truck truckB2 = new Truck(2, 2);
            Truck truckB3 = new Truck(2, 3);
            Truck truckB4 = new Truck(2, 4);
            Truck truckB5 = new Truck(2, 5);

            // Create a greedy starting solution
            truckA1.GreedyHandleOrder();
            truckA3.GreedyHandleOrder();
            truckA2.GreedyHandleOrder();
            truckA5.GreedyHandleOrder();
            truckA4.GreedyHandleOrder();
            truckB1.GreedyHandleOrder();
            truckB2.GreedyHandleOrder();
            truckB3.GreedyHandleOrder();
            truckB4.GreedyHandleOrder();
            truckB5.GreedyHandleOrder();

            schedule.daySchedules[0] = truckA1.day;
            schedule.daySchedules[1] = truckA2.day;
            schedule.daySchedules[2] = truckA3.day;
            schedule.daySchedules[3] = truckA4.day;
            schedule.daySchedules[4] = truckA5.day;
            schedule.daySchedules[5] = truckB1.day;
            schedule.daySchedules[6] = truckB2.day;
            schedule.daySchedules[7] = truckB3.day;
            schedule.daySchedules[8] = truckB4.day;
            schedule.daySchedules[9] = truckB5.day;

            schedule.Declined();

            // Start Simulated Annealing
            int iteration = 0;

            temperature = 200.0;

            // Execute iterations until epsilon is reached          
            while (temperature > epsilon)
            {
                iteration++;

                // Get the next random permutation of distances 
                RandomOperator();
                
                // Reduce the temperature
                if (iteration % 100 == 0)
                {
                    temperature *= alfa;
                }                

                //if (iteration % 100000 == 0)
                //{
                //    schedule.RemoveUselessOrders();
                //}

            }
            
            schedule.RemoveUselessOrders();

            schedule.daySchedules[0].route.Print();
            schedule.daySchedules[1].route.Print();
            schedule.daySchedules[2].route.Print();
            schedule.daySchedules[3].route.Print();
            schedule.daySchedules[4].route.Print();

            schedule.daySchedules[5].route.Print();
            schedule.daySchedules[6].route.Print();
            schedule.daySchedules[7].route.Print();
            schedule.daySchedules[8].route.Print();
            schedule.daySchedules[9].route.Print();

            Console.WriteLine(schedule.Score() / 60f);

            //Console.WriteLine("Done");
            Console.ReadLine();
        }

        // Excecutes one of five operations
        public static void RandomOperator()
        {
            int o = random.Next(0, 5);

            switch (o)
            {
                case 0:
                    sa.Add();
                    break;
                case 1:
                    sa.Remove();
                    break;
                case 2:
                    sa.SwapNeighbors();
                    break;
                case 3:
                    sa.SwapLocal();
                    break;
                case 4:
                    sa.SwapGlobal();
                    break;
            }
        }

        // Fill a HashSet in which the orders are saved
        public static void FillOrderList(string[] linesOrder)
        {
            bool orderHeader = true;

            foreach (string orderString in linesOrder)
            {
                if (orderHeader)
                {
                    orderHeader = false;
                    continue;
                }

                string[] orderSplit = orderString.Split(';');
                Order order = new Order(int.Parse(orderSplit[0]),
                                        orderSplit[1].Trim(),
                                        (int)Char.GetNumericValue(orderSplit[2][0]),
                                        int.Parse(orderSplit[3]),
                                        int.Parse(orderSplit[4]),
                                        float.Parse(orderSplit[5], CultureInfo.InvariantCulture),
                                        int.Parse(orderSplit[6]),
                                        int.Parse(orderSplit[7]),
                                        int.Parse(orderSplit[8]));
                orderList.Add(order);
            }
        }
        

        // Fill a Array in which the distances are saved. Afstandenmatrix[matrixID1,matrixID2] == Rijtijd van 1 naar 2
        public static void FillAfstandenMatrix(string[] linesAfstanden)
        {
            bool afstandenHeader = true;            

            foreach (string afstandenString in linesAfstanden)
            {
                if (afstandenHeader)
                {
                    afstandenHeader = false;
                    continue;
                }

                string[] afstandenSplit = afstandenString.Split(';');
                int matrixID1 = int.Parse(afstandenSplit[0]);
                int matrixID2 = int.Parse(afstandenSplit[1]);

                afstandenMatrix[matrixID1, matrixID2] = int.Parse(afstandenSplit[3]); // rijtijd in seconden
            }
        }

    }
}
