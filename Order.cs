﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GroteOpdracht
{
    class Order
    {
        public int orderID;
        public string town;
        public int frequency;
        public int containers;
        public int containerVolume;
        public int totalVolume;
        public float emptyingTime; // seconds
        public int matrixID;
        public int x;
        public int y;        
        public int timesVisited; // the amount of times an order is visited
        public bool[] completedOn;

        public Order(int orderID, string town, int frequency, int containers, int containerVolume, float emptyingTime, int matrixID, int x, int y)
        {
            this.orderID = orderID;
            this.town = town;
            this.frequency = frequency;
            this.containers = containers;
            this.containerVolume = containerVolume;
            this.totalVolume = containers * containerVolume;
            this.emptyingTime = emptyingTime * 60f; // in seconden
            this.matrixID = matrixID;
            this.x = x;
            this.y = y;
            this.timesVisited = 0;
            this.completedOn = new bool[5] { false, false, false, false, false}; 
        }

        // Dump Location
        public Order(int matrixID)
        {
            this.orderID = 0; // stortplaats heeft ID = 0
            this.town = "MAARHEEZE";
            this.frequency = 0;
            this.emptyingTime = 1800f; // in seconden
            this.matrixID = matrixID;
            this.x = 56343016;
            this.y = 513026712;
        }

        // Returns true if two orders are the same
        public bool Equals(Order order)
        {
            return matrixID == order.matrixID && orderID == order.orderID;
        }

        // Given a day, returns true if the order has been completed on three or less different days
        public bool CompletedThreeOrLessTimes(int dayNumber)
        {
            int count = 0;

            if (completedOn[dayNumber - 1])
            {
                return false; // the order is already completed on this day
            }

            for (int i = 1; i <= 5; i++)
            {
                if (completedOn[i - 1])
                {
                    count++;
                }
            }

            return count <= 3;
        }


        // Returns true if an order is completed on only 2 given day numbers, used for frequency checks
        public bool CompletedOnlyOn(int dayNumber1, int dayNumber2)
        {
            bool result = true;

            for (int i = 1; i <= 5; i++)
            {
                if (i == dayNumber1 || i == dayNumber2)
                {
                    result = result && completedOn[i - 1];
                }
                else
                {
                    result = result && !completedOn[i - 1];
                }
            }

            return result;
        }


        // Returns true if an order is completed on only 1 given day number, used for frequency checks
        public bool CompletedOnlyOn(int dayNumber)
        {
            bool result = true;

            for (int i = 1; i <= 5; i++)
            {
                if (i == dayNumber)
                {
                    result = result && completedOn[i - 1];
                }
                else
                {
                    result = result && !completedOn[i - 1];
                }
            }

            return result; 
        }

        // Returns true if an order is completed on exactly one day
        public bool CompletedOnOne()
        {
            int count = 0;

            for (int i = 1; i <= 5; i++)
            {
                if (completedOn[i - 1])
                {
                    count++;
                }
            }

            return count == 1;
        }

        // Returns true if an order is not (yet) completed on any day
        public bool NeverCompleted()
        {
            bool result = true;

            for (int i = 1; i <= 5; i++)
            {
                result = result && !completedOn[i - 1];
            }

            return result;
        }

        public void SetFlagTrue (int dayNumber)
        {
            if (completedOn[dayNumber - 1])
            {
                //throw new Exception("SetFlagTrue called, but flag is already true.");
            }
            else
            {
                completedOn[dayNumber - 1] = true;
            }
        }

        public void SetFlagFalse(int dayNumber)
        {
            if (!completedOn[dayNumber - 1])
            {
                //throw new Exception("SetFlagFalse called, but flag is already false.");
            }
            else
            {
                completedOn[dayNumber - 1] = false;
            }
        }
    }
}
